# This file contains some common imports here we use to import registered configs.
from official.projects.deepmac_maskrcnn.tasks import deep_mask_head_rcnn
from seminar.models.vision.configs import maskrcnn_config
from seminar.models.vision.configs import image_classification
# from seminar.models.vision.tasks import maskrcnn_task
# from seminar.models.vision.tasks import image_classification
