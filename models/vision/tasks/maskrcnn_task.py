"""MaskRCNN task definition."""
from official.core import task_factory
from official.vision.configs import maskrcnn as exp_cfg

from seminar.models.vision.configs import maskrcnn_config

@task_factory.register_task_cls(maskrcnn_config.MaskHeadRCNNTask)
class DeepMaskHeadRCNNTask(exp_cfg.MaskRCNNTask):
    """ Here we can override task functions if needed
    - build_inputs
    - build_model
    - build_losses
    - build_metrics
    - train_step"""

    pass
