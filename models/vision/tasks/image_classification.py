"""Image classification task definition."""
from official.core import task_factory
from official.vision.configs import image_classification as exp_cfg

from seminar.models.vision.configs import image_classification as classification_config

@task_factory.register_task_cls(classification_config.ClassificationTask)
class ClassificationTask(exp_cfg.ImageClassificationTask):
    """ Here we can override task functions if needed
    - build_inputs
    - build_model
    - build_losses
    - build_metrics
    - train_step"""

    pass
