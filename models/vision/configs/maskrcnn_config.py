"""Configuration for Mask R-CNN based model."""
import yaml

from official.core import exp_factory
from official.projects.deepmac_maskrcnn.configs import deep_mask_head_rcnn as deepmaskrcnn_config
from official.vision.configs import maskrcnn as maskrcnn_config
from official.core import config_definitions as cfg


@exp_factory.register_config_factory('maskrcnn')
def maskrcnn() -> cfg.ExperimentConfig():
    """Experiment config for maskrcnn model."""
    config = maskrcnn_config.maskrcnn_resnetfpn_coco()
    yaml_path = './seminar/models/vision/configs/experiments/experiment_maskrcnn.yaml'
    with open(yaml_path, encoding="utf-8") as file:
        params = yaml.safe_load(file)
        config.override(params, is_strict=False)
    return config

@exp_factory.register_config_factory('deepmaskrcnn')
def deepmaskrcnn() -> cfg.ExperimentConfig:
    """Experiment config for deepmaskrcnn model using yaml."""
    config = deepmaskrcnn_config.deep_mask_head_rcnn_resnetfpn_coco()
    yaml_path = './seminar/models/vision/configs/experiments/experiment_maskrcnn.yaml'
    with open(yaml_path, encoding="utf-8") as file:
        params = yaml.safe_load(file)
        config.override(params, is_strict=False)
    return config

@exp_factory.register_config_factory('deepmaskrcnn_method2')
def new_deepmask_rcnn_config() -> cfg.ExperimentConfig:
    """Function to load and iverride particular params of experiment config."""
    config = deepmaskrcnn_config.deep_mask_head_rcnn_resnetfpn_coco()

    config.task.model.input_size = [64, 64, 3]

    config.task.train_data.global_batch_size = 1
    config.task.validation_data.global_batch_size = 1

    config.task.train_data.shuffle_buffer_size = 1
    config.task.validation_data.shuffle_buffer_size = 1

    config.trainer.steps_per_loop = 5
    config.trainer.summary_interval = 5

    config.trainer.train_steps = 5

    config.task.export_checkpoint_subdir = 'checkpoint'
    config.task.export_saved_model_subdir = 'saved_model'

    config.task.init_checkpoint = './seminar/checkpoints/deepmaskrcnn/best_ckpt_23'
    config.task.init_checkpoint_modules = 'all'

    return config
