"""Configuration for Classification based model."""
import yaml

from official.core import exp_factory
from official.vision.configs import image_classification
from official.core import config_definitions as cfg


@exp_factory.register_config_factory('resnet50')
def resnet50() -> cfg.ExperimentConfig():
    """Experiment config for image classification with resnet50 model."""
    config = image_classification.image_classification_imagenet()
    yaml_path = './seminar/models/vision/configs/experiments/experiment_classification.yaml'
    with open(yaml_path, encoding="utf-8") as file:
        params = yaml.safe_load(file)
        config.override(params, is_strict=False)
    return config

@exp_factory.register_config_factory("resnetrs50")
def resnet50_imagenet() -> cfg.ExperimentConfig:
    """Experiment config for image classification with resnetrs50 model."""
    config = image_classification.image_classification_imagenet_resnetrs()
    yaml_path = './seminar/models/vision/configs/experiments/experiment_classification.yaml'
    with open(yaml_path, encoding="utf-8") as file:
        params = yaml.safe_load(file)
        config.override(params, is_strict=False)
    return config
