# we add code that export the model from this file

# one of the way to export model
# tfm.vision.serving.export_saved_model_lib.export_inference_graph

# we can create export module and then export that module
# By default we have detection module in the serving of official.models.vision
# we can override and change the detection module according to our use case.

from absl import app
import tensorflow_models as tfm

from official.core import exp_factory
from official.vision.serving import detection

from seminar.models.vision.common import registry_import  #pylint: disable=unused-import

def main(_):
    """ Used to export model."""
    config = exp_factory.get_exp_config('deepmaskrcnn')
    checkpoint_path = './seminar/checkpoints/deepmaskrcnn/best_ckpt_23'
    export_dir = './seminar/exported_models/'

    export_module = detection.DetectionModule(
        params=config,
        batch_size=None,
        input_image_size=config.task.model.input_size[:2],
        num_channels=3
    )

    tfm.vision.serving.export_saved_model_lib.export_inference_graph(
      input_type='image_tensor',
      batch_size=None,
      input_image_size=config.task.model.input_size[:2],
      params=config,
      checkpoint_path=checkpoint_path,
      export_dir=export_dir,
      export_checkpoint_subdir='checkpoint',
      export_saved_model_subdir='saved_model',
      export_module=export_module,
      log_model_flops_and_params=False,
      input_name=None)

if __name__ == '__main__':
    app.run(main)
