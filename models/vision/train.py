# To train the model we need to follow the following steps:

# 1. Get the config
# 2. Create the task
# 3. run experiment

# To load config use get_exp_config({registered_name})
# To get task from the config get_task(config.task)
# to run experiment we use train_lib.run_experiment (train_lib is present in official.core)

from absl import app
import tensorflow as tf
from official.core import exp_factory, task_factory, train_lib

from seminar.models.vision.common import registry_import #pylint: disable=unused-import

def main(_):
    """Loads the config and trains the model. """
    config = exp_factory.get_exp_config('maskrcnn')
    task = task_factory.get_task(config.task)

    # this directory stores all the logs during the training
    model_dir = './models/training_models/'
    train_lib.run_experiment(
        distribution_strategy=tf.distribute.get_strategy(),
        task=task,
        mode='train',
        params=config,
        model_dir=model_dir,
    )

if __name__ == '__main__':
    app.run(main)
